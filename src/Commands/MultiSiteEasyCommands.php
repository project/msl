<?php

namespace Drupal\msl\Commands;

use Drush\Commands\DrushCommands;

use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Output\ConsoleOutput;

/**
 * A Drush commandfile.
 *
 * @category Msl
 * @package Msl Module
 * @link https://www.drupal.org/project/msl
 */
class MultiSiteEasyCommands extends DrushCommands {

  /**
   * Custom Drush commands made easy to work with multisite setup.
   *
   * @param string $params
   *   A space-separated list of parameters.
   *   Use quotes to pass an array of parameters and
   *   backslash to escape special characters.
   *   Example: drush msl "\-r \-l \-v etc...".
   * @param array $options
   *   An array of options to use during processing.
   *
   * @command msl
   * @aliases multi-site-list
   *
   * @option save Select a site to save(use this site as default).
   * @option clear Clear the site saved as default.
   * @option remove Remove the site from config data.
   * @option opt A key-value pair of drush command.
   *   Example: --opt=foo=bar --opt=baz=qux
   * @option opts A comma-separated list of key-value pairs.
   *   Use quotes to pass an array of options.
   *   Example: --opts="uri=https://example.com,foo=bar,baz=qux"
   * @option add A comma-separated list of key-value(site_uri,site_name) pairs.
   *   Use quotes and separate url and site name by comma. Or just --add.
   *   Example: --add="https://example.com,example"
   *
   * @validate-module-enabled msl
   * @usage mycommand "[<params>]" [--<opt>=<key>=<value>]...
   * @bootstrap full
   */
  public function multiSite(
    $params = '',
    $options = ['opt' => [], 'opts' => '', 'add' => '']
  ) {
    $this->output()->writeln(
      "<options=bold;bg=cyan;fg=black>\n[Notice]</><options=bold> If you are using commands and special characters like '$' and '-r, -d, -v, etc'. 
      Please add '\' before '-' and '$'.
      Eg: drush msl '\-r /var/www/html status'</>");

    // Initialising variables.
    $optionValues = '';
    $addSiteUrllist = [];
    $addSiteNamelist = [];

    $params = stripslashes($params);
    $set_drush_command = "drush " . $params;

    // Processing opts parameter.
    if (!empty($options['opts']) && $options['opts'] !== TRUE) {
      $getOptsCmds = explode(',', $options['opts']);
      $options['opt'] = array_merge($options['opt'], $getOptsCmds);
    }

    // Processing opt parameter.
    if (!empty($options['opt'])) {
      $optionPairs = $options['opt'];
      foreach ($optionPairs as $optionPair) {
        if (str_contains($optionPair, '=')) {
          [$optionName, $optionValue] = explode('=', $optionPair, 2);
          $optionValues .= '--' . $optionName . '=' . $optionValue . ' ';
        }
        else {
          $optionValues .= '--' . $optionPair . ' ';
        }
      }
    }

    // Processing remove parameter, if yes removing site from config data.
    if ($options['remove']) {
      self::removeSiteFromConfigData();
      return;
    }

    // Processing add parameter, if yes adding site to config data.
    self::addNewSiteToConfig($options, FALSE);

    // Path to store the sites fetched from sites.php.
    $module_handler = \Drupal::moduleHandler();
    $pathToMyModule = $module_handler->getModule('msl')->getPath();
    $sites_copyfile_path = $pathToMyModule . '/fetch_sites.php';

    // Processing -l and --uri in command.
    if (str_contains($params, '-l')) {
      $this->output()->writeln("<comment>\nFound '-l' in command, not modifying command. 
      Please remove it to select from list of sites</comment>");
      passthru($set_drush_command);
    }
    elseif (str_contains($params, '--uri')) {
      $this->output()->writeln("<comment>\nFound '--uri' in command, not modifying command.
      Please remove it to select from list of sites</comment>");
      passthru($set_drush_command);
    }
    else {

      // Fetch Sites from sites.php and config data.
      $sites = self::fetchSites($sites_copyfile_path);
      $sites = array_merge($sites, self::fetchSitesFromConfig());

      // Processing clear parameter.
      if ($options['clear']) {
        \Drupal::state()->delete('persist_url');
      }

      // Processing save (getting value from terminal session 'persist_url'
      // if present) parameter.
      $persist_url = \Drupal::state()->get('persist_url', NULL);
      if ($persist_url !== NULL) {
        // Using session value if present.
        $set_drush_command .= " --uri=" . $persist_url;
        $this->output()->writeln("<comment>\nUse --clear to clear memory and select different URL.</comment>");
      }
      else {
        // No session value, ask user to select.
        $set_drush_command = self::selectSiteFromList($sites, $set_drush_command, $options);
      }
      // Setting final drush command and run it.
      $set_drush_command .= ' ' . $optionValues;
      $this->output()->writeln("<info>\nRunning drush command:</info> $set_drush_command \n");
      passthru($set_drush_command);
    }

  }

  /**
   * Add new site and store in config.
   */
  public function addNewSiteToConfig($options, $select0 = FALSE) {
    $config = \Drupal::configFactory()->getEditable('msl.settings');
    $addSiteToConfig = $config->get('sites') ?? [];

    // If add opt is string, explode it to get url and name of site.
    if (is_string($options['add']) && $options['add'] !== '' && $options['add'] !== TRUE) {
      // If comma present get site url and name.
      if (str_contains($options['add'], ',')) {
        [$addSiteUrl, $addSiteName] = explode(',', $options['add'], 2);
      }
      // If no comma, ask for site name.
      else {
        $addSiteUrl = $options['add'];
        $addSiteName = $this->io()->ask('Please enter site name ');
      }
      $addSiteToConfig[] = [
        'url' => $addSiteUrl,
        'name' => $addSiteName,
      ];

      // Saving new site to config.
      $config->set('sites', $addSiteToConfig)->save();
      if (isset($addSiteUrl)) {
        $this->output()->writeln(
          "<info>\nSuccessfully added <href='$addSiteUrl'>$addSiteUrl</> to the list.</info>");
      }
    }
    // Select0 is to check if this function is called from another function,
    // or if add parameter is passed as bool in command.
    elseif ((is_bool($options['add']) && $options['add'] == TRUE) || $select0 == TRUE) {
      // Getting input from user.
      $addSiteUrl = $this->io()->ask('Please enter --uri ');
      $addSiteName = $this->io()->ask('Please enter site name ');
      $addSiteToConfig[] = [
        'url' => $addSiteUrl,
        'name' => $addSiteName,
      ];
      $config->set('sites', $addSiteToConfig)->save();
      if (isset($addSiteUrl)) {
        $this->output()->writeln(
          "<info>\nSuccessfully added <href='$addSiteUrl'>$addSiteUrl</> to the list.</info>");
      }
      return $addSiteUrl;
    }
  }

  /**
   * Remove site from config.
   */
  public function removeSiteFromConfigData() {
    $config = \Drupal::configFactory()->getEditable('msl.settings');
    $removeSiteFromConfig = $config->get('sites') ?? [];

    $keys = array_keys($removeSiteFromConfig);
    // Printing in table format in terminal.
    $table = new Table(new ConsoleOutput());
    $table->setHeaders(['#', 'Site URL', 'Site Name']);
    // First option to remove all sites.
    $table->addRow(['<options=bold>0</>', '<options=bold>Remove all sites</>']);
    foreach ($keys as $index => $key) {
      $configSiteUrl = $removeSiteFromConfig[$key]['url'];
      $configSiteName = $removeSiteFromConfig[$key]['name'];
      $table->addRow([$index + 1, $configSiteUrl, $configSiteName]);
    }
    $table->render();

    // Get user input.
    $selectedOptionIndex = $this->io()->ask('Enter the number of your choice ');
    if ($selectedOptionIndex == 0) {
      $config->clear('sites')->save();

      $this->output()->writeln("<comment>\nCleared all sites.</comment>");
    }
    else {
      $selectedOption = $keys[$selectedOptionIndex - 1];

      unset($removeSiteFromConfig[$selectedOption]);
      $config->set('sites', $removeSiteFromConfig)->save();

      $this->output()->writeln("<comment>\nSuccessfully removed site number $selectedOptionIndex.</comment>");
    }
  }

  /**
   * Fetch sites stored in config.
   */
  public function fetchSitesFromConfig() {
    $config = \Drupal::config('msl.settings');
    $sitesFromConfig = $config->get('sites');
    $returnSites = [];
    foreach ($sitesFromConfig as $index => $site) {
      $returnSites[$site['url']] = $site['name'];
    }
    return $returnSites;
  }

  /**
   * Fetch sites from sites.php.
   */
  public function fetchSites($sites_copyfile_path) {
    $this->output()->writeln("<comment>\nIf sites present in your root_DIR/sites/sites.php and are not shown here, 
    please check file access permission.</comment>");
    $sites = NULL;
    $sites_path = DRUPAL_ROOT . '/sites/sites.php';
    // Check if sites.php present.
    if (file_exists($sites_path)) {
      $contents = file_get_contents($sites_path);
      // Copying content from sites.php to a temporary file.
      if (!file_exists($sites_copyfile_path)) {
        $sitesFileHandle = fopen($sites_copyfile_path, 'w') or die("can't open file");
        fclose($sitesFileHandle);
      }
      file_put_contents($sites_copyfile_path, $contents);
    }
    // Once sites are fetched, deleting the temporary file.
    include_once $sites_copyfile_path;
    $sites = $sites;
    unlink($sites_copyfile_path);
    return $sites;
  }

  /**
   * Select which site to run the drush command.
   */
  public function selectSiteFromList($sites, $set_drush_command, $options) {
    try {
      if ($sites == NULL) {
        $this->output()->writeln("<comment>\nCouldn't find any sites, 
        Please enter site url and name to run the given command.</comment>");

        // If no sites found, call function to add site.
        $selectedOption = self::addNewSiteToConfig($options, TRUE);
      }
      else {
        // Printing Site options for user to select in table format.
        $keys = array_keys($sites);
        $table = new Table(new ConsoleOutput());
        $table->setHeaders(['#', 'Site URL', 'Site Name']);
        // First option to add new site.
        $table->addRow(['<options=bold>0</>', '<options=bold>Enter New Site</>']);
        foreach ($keys as $index => $key) {
          $value = $sites[$key];
          $table->addRow([$index + 1, $key, $value]);
        }
        $table->render();

        $this->output()->writeln("<comment><options=bold;bg=yellow;fg=black>[Note]</> Press Enter to run command on main site.</comment>");
        $selectedOptionIndex = $this->io()->ask('Enter the number of your choice ');
        if ($selectedOptionIndex == 0) {
          // If user input 0, call function to add site.
          $selectedOption = self::addNewSiteToConfig($options, TRUE);
        }
        else {
          $selectedOption = $keys[$selectedOptionIndex - 1];

          $this->output()->writeln("<info>You selected:</info> $selectedOption");
        }
      }
      // If --save flag passed, store the selected site to session.
      if ($options['save']) {
        \Drupal::state()->set('persist_url', $selectedOption);
      }
      $persist_url = \Drupal::state()->get('persist_url', NULL);
      if ($persist_url !== NULL) {
        $set_drush_command .= " --uri=" . $persist_url;
      }
      elseif ($selectedOption !== NULL) {
        $set_drush_command .= " --uri=" . $selectedOption;
      }
      return $set_drush_command;
    }
    catch (Exception $e) {
      $this->output()->write("<error>\nError: " . $e->getMessage() . "</error>");
      return $set_drush_command;
    }
  }

}
